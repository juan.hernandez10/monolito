# Proyecto Plan de Crecimiento

Proyecto hecho con el fin de validar los conceptos adquiridos en la capacitacion Java, Spring Framework, Spring Boot y microservicios. Se busca construir una API que exponga operaciones CRUD de los clientes con su respectiva imagen.

## Nombre
Api rest monolitica de cliente.

## Descripcion
Se obtiene y manipula la informacion de los clientes por medio de una api rest hecha en java con el framework springBoot,la informacion de cliente es la siguiente: 
- [ ] Nombres
- [ ] Apellidos
- [ ] Tipo y numero de documento
- [ ] Edad
- [ ] Ciudad de nacimiento
- [ ] Foto (archivo base64)
