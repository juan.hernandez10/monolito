package com.monolito.demo.repositorios;

import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.monolito.demo.modelo.Cliente;

@Repository
public interface Rcliente extends JpaRepository<Cliente, Integer> {
	Cliente findByNumeroDoc(int numeroDoc);
	List<Cliente> findByTipDocumentoTipDoc(String tipDoc);
	List<Cliente> findByEdadGreaterThanEqual(int edad);
}