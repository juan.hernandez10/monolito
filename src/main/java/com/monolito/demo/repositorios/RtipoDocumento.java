package com.monolito.demo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import com.monolito.demo.modelo.TipoDocumento;
import org.springframework.stereotype.Repository;

@Repository
public interface RtipoDocumento extends JpaRepository<TipoDocumento, Integer>{
	TipoDocumento findByTipDoc(String tipDoc);
}
