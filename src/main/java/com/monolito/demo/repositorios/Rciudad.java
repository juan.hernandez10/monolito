package com.monolito.demo.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import com.monolito.demo.modelo.Ciudad;
import org.springframework.stereotype.Repository;

@Repository
public interface Rciudad extends JpaRepository<Ciudad, Integer>{
	Ciudad findByNombre(String nombre);
}
