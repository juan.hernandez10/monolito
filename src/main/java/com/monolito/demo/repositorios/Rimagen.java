package com.monolito.demo.repositorios;

import com.monolito.demo.modelo.Imagen;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Rimagen extends MongoRepository<Imagen, String>{
    Imagen findByNumeroDoc(int numeroDoc);
}
