package com.monolito.demo.servicios;

import com.monolito.demo.dto.ImagenDto;
import com.monolito.demo.excepciones.ErrorGeneral;
import com.monolito.demo.mapeador.Mapeador;
import com.monolito.demo.repositorios.Rcliente;
import com.monolito.demo.repositorios.Rimagen;
import com.monolito.demo.modelo.Imagen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.Base64;

@Service
public class Simagen{
    @Autowired
    private Rimagen imagenS;

    @Autowired
    private Rcliente clienteS;

    @Autowired
    private Mapeador mapeador;

    private static final String RUTAAGREGAR = "/imagen/agregar";
    private static final String ERRCLIENTENOEXISTE = "El Cliente no existe.";
    private static final String ERRORCLIENTENOIMAGEN = "El Cliente no posee imagen.";

    public String codificarImagen(MultipartFile imagen){
        try {
            return Base64.getEncoder().encodeToString(imagen.getBytes());
        } catch (IOException e) {
            return  null;
        }
    }


    @Transactional(readOnly = true)
    public ResponseEntity<Object> obtenerImagenId(int numeroDoc) {
        if(clienteS.findByNumeroDoc(numeroDoc) != null){
            Imagen imagen = imagenS.findByNumeroDoc(numeroDoc);
            if(imagen != null){
                ImagenDto imagenDto = mapeador.toImagenDto(imagen);
                return new ResponseEntity<>(imagenDto, HttpStatus.OK);
            }
            return new ResponseEntity<>(new ErrorGeneral(404, ERRORCLIENTENOIMAGEN,
                                  "/documento/"+numeroDoc), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ErrorGeneral(400, ERRCLIENTENOEXISTE,
                              "/documento/"+numeroDoc), HttpStatus.BAD_REQUEST);
    }

    @Transactional(readOnly = false)
    public ResponseEntity<Object> agregarImagen(Integer numeroDoc, MultipartFile file){
        if(clienteS.findByNumeroDoc(numeroDoc) != null) {
            if (imagenS.findByNumeroDoc(numeroDoc) == null) {
                Imagen nuevaImagen = new Imagen(numeroDoc, codificarImagen(file));
                imagenS.save(nuevaImagen);
                return new ResponseEntity<>(nuevaImagen, HttpStatus.CREATED);
            }
            return new ResponseEntity<>(new ErrorGeneral(400, "El cliente ya posee imagen",
                                            RUTAAGREGAR), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new ErrorGeneral(400, ERRCLIENTENOEXISTE,
                                        RUTAAGREGAR), HttpStatus.BAD_REQUEST);
    }

    @Transactional(readOnly = false)
    public ResponseEntity<Object> eliminarImagen(int numeroDoc) {
        if(clienteS.findByNumeroDoc(numeroDoc) != null){
            Imagen imagenTemp = imagenS.findByNumeroDoc(numeroDoc);
            if(imagenTemp != null){
                imagenS.deleteById(imagenTemp.getId());
                return new ResponseEntity<>(imagenTemp, HttpStatus.OK);
            }
            return new ResponseEntity<>(new ErrorGeneral(404, ERRORCLIENTENOIMAGEN,
                    "/eliminar/"+numeroDoc), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ErrorGeneral(400, ERRCLIENTENOEXISTE,
                "/eliminar/"+numeroDoc), HttpStatus.BAD_REQUEST);
    }

    @Transactional(readOnly = false)
    public ResponseEntity<Object> actualizarImagen(int numeroDoc, MultipartFile file) {
        if(clienteS.findByNumeroDoc(numeroDoc) != null){
            Imagen imagenTemp = imagenS.findByNumeroDoc(numeroDoc);
            if(imagenTemp != null){
                imagenTemp.setImagenCliente(codificarImagen(file));
                imagenS.save(imagenTemp);
                return new ResponseEntity<>(imagenTemp, HttpStatus.OK);
            }
            return new ResponseEntity<>(new ErrorGeneral(404, ERRORCLIENTENOIMAGEN,
                    "/actualizar"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ErrorGeneral(400, ERRCLIENTENOEXISTE,
                "/actualizar"), HttpStatus.BAD_REQUEST);
    }
}
