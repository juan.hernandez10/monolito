package com.monolito.demo.servicios;

import java.util.List;
import com.monolito.demo.dto.AgregarClienteDto;
import com.monolito.demo.dto.ActualizarClienteDto;
import com.monolito.demo.dto.ClienteImagenDto;
import com.monolito.demo.mapeador.Mapeador;
import com.monolito.demo.modelo.Imagen;
import com.monolito.demo.repositorios.Rimagen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.monolito.demo.repositorios.Rcliente;
import com.monolito.demo.repositorios.Rciudad;
import com.monolito.demo.repositorios.RtipoDocumento;
import com.monolito.demo.modelo.Ciudad;
import com.monolito.demo.modelo.Cliente;
import com.monolito.demo.modelo.TipoDocumento;
import com.monolito.demo.excepciones.ErrorGeneral;

@Service
public class Scliente{

	@Autowired
	private Rcliente clienteS;

	@Autowired
	private Rimagen imagenS;
	
	@Autowired
	private Rciudad ciudadS;

	@Autowired
	private Mapeador mapeador;
	
	@Autowired
	private RtipoDocumento tipoDocS;
	private static final String MENSAJEERROR = "El cliente con el numero de documento ";
	

	@Transactional(readOnly = true)
	public ResponseEntity<Object> listarClientesId(int numDoc){
		Cliente cliente = clienteS.findByNumeroDoc(numDoc);
		if(cliente != null){
			ClienteImagenDto clienteImagenDto = mapeador.toClienteImagenDto(cliente);
			Imagen imagen = imagenS.findByNumeroDoc(numDoc);
			if(imagen != null) {
				clienteImagenDto.setImagen(imagen.getImagenCliente());
				return new ResponseEntity<>(clienteImagenDto, HttpStatus.OK);
			}
			return new ResponseEntity<>(clienteImagenDto, HttpStatus.OK);
		}
		return new ResponseEntity<>(new ErrorGeneral(404, "No existe cliente con ese id", 
				                    "cliente/documento/"+numDoc), HttpStatus.NOT_FOUND);
	}

	@Transactional(readOnly = true)
	public ResponseEntity<Object> listarClientesTipo(String tipDoc){
		List<Cliente> clientes = clienteS.findByTipDocumentoTipDoc(tipDoc);
		if(!clientes.isEmpty()){
			List<ClienteImagenDto> clienteImagenDtos = mapeador.toClienteImagenDtos(clientes);
			clienteImagenDtos = agregarImagenes(clienteImagenDtos);
			return new ResponseEntity<>(clienteImagenDtos, HttpStatus.OK);
		}
		return new ResponseEntity<>(new ErrorGeneral(404, "No existe clientes con ese tipo de documento", 
                                    "cliente/tipDoc/"+tipDoc), HttpStatus.NOT_FOUND);
	}

	public ResponseEntity<Object> listarClientesEdad(int edad) {
		List<Cliente> clientes = clienteS.findByEdadGreaterThanEqual(edad);
		if(!clientes.isEmpty()){
			List<ClienteImagenDto> clienteImagenDtos = mapeador.toClienteImagenDtos(clientes);
			clienteImagenDtos = agregarImagenes(clienteImagenDtos);
			return new ResponseEntity<>(clienteImagenDtos, HttpStatus.OK);
		}
		return new ResponseEntity<>(new ErrorGeneral(404, "No existe clientes con una edad mayor o igual a "
		                            +edad, "cliente/edad/"+edad), HttpStatus.NOT_FOUND);
	}

	@Transactional(readOnly = false)
	public ResponseEntity<Object> agregarCliente(AgregarClienteDto nClienteDto){
		if(clienteS.findByNumeroDoc(nClienteDto.getNumeroDoc()) == null){
			Ciudad idCiudad = ciudadS.findByNombre(nClienteDto.getCiudad());
			TipoDocumento idTipoDoc = tipoDocS.findByTipDoc(nClienteDto.getTipDocumento());
			Cliente clienteTemp = mapeador.toCliente(nClienteDto);
			clienteTemp.setCodCiudad(idCiudad);
			clienteTemp.setTipDocumento(idTipoDoc);
			clienteS.save(clienteTemp);
			return new ResponseEntity<>(clienteTemp, HttpStatus.CREATED);
		}
		return new ResponseEntity<>(new ErrorGeneral(400, MENSAJEERROR+nClienteDto.getNumeroDoc()
									+" ya existe.", "/cliente/agregar"), HttpStatus.BAD_REQUEST);
	}

	@Transactional(readOnly = false)
	public ResponseEntity<Object> eliminarCliente(Integer numDoc){
		Cliente clienteDel = clienteS.findByNumeroDoc(numDoc);
		if(clienteDel != null){
			Imagen imagenTemp = imagenS.findByNumeroDoc(numDoc);
			if(imagenTemp != null){
				imagenS.deleteById(imagenTemp.getId());
			}
			clienteS.deleteById(numDoc);
			return new ResponseEntity<>(clienteDel, HttpStatus.OK);
		}
		else{
			return new ResponseEntity<>(new ErrorGeneral(404, MENSAJEERROR+numDoc+" no existe.", 
					                    "/cliente/eliminar/"+numDoc), HttpStatus.NOT_FOUND);
		}
	}

	@Transactional(readOnly = false)
	public ResponseEntity<Object> actualizarCliente(ActualizarClienteDto actClienteDto, Integer numeroDoc){
		Cliente clienteAct = clienteS.findByNumeroDoc(numeroDoc);
		if(clienteAct != null){
			clienteAct.setNombres(actClienteDto.getNombres());
			clienteAct.setApellidos(actClienteDto.getApellidos());
			clienteS.save(clienteAct);
			return new ResponseEntity<>(clienteAct, HttpStatus.CREATED);
		}
		return new ResponseEntity<>(new ErrorGeneral(404, MENSAJEERROR+numeroDoc
		                            +" no existe.", "/cliente/actualizar/"+numeroDoc), HttpStatus.NOT_FOUND);
	}

	public List<ClienteImagenDto> agregarImagenes(List<ClienteImagenDto> clientes) {
		for(ClienteImagenDto i: clientes){
			Imagen imagenTemp = imagenS.findByNumeroDoc(i.getNumeroDoc());
			if(imagenTemp != null){
				i.setImagen(imagenTemp.getImagenCliente());
			}
		}
		return clientes;
	}
}
