package com.monolito.demo.dto;

public class ClienteImagenDto {
    private Integer numeroDoc;
    private String nombres;
    private String apellidos;
    private String tipDocumento;
    private String ciudad;
    private int edad;
    private String imagen;

    public ClienteImagenDto() {
    }

    public ClienteImagenDto(Integer numeroDoc, String nombres, String apellidos, String tipDocumento, String ciudad,
                            int edad, String imagen) {
        super();
        this.numeroDoc = numeroDoc;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.tipDocumento = tipDocumento;
        this.ciudad = ciudad;
        this.edad = edad;
        this.imagen = imagen;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Integer getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(Integer numeroDoc) {
        this.numeroDoc = numeroDoc;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTipDocumento() {
        return tipDocumento;
    }

    public void setTipDocumento(String tipDocumento) {
        this.tipDocumento = tipDocumento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}
