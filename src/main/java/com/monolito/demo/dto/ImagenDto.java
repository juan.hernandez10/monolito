package com.monolito.demo.dto;

public class ImagenDto{
    private String id;
    private int numeroDoc;
    private String imagen;

    public ImagenDto(String id, int numeroDoc, String imagen) {
        this.id = id;
        this.numeroDoc = numeroDoc;
        this.imagen = imagen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(int numeroDoc) {
        this.numeroDoc = numeroDoc;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
