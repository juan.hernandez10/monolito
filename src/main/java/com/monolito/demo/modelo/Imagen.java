package com.monolito.demo.modelo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "imagenesCliente")
public class Imagen {
    @Id
    private String id;

    @Indexed
    private int numeroDoc;
    private String imagenCliente;

    public Imagen() {
    }

    public Imagen(int numeroDoc, String imagenCliente) {
        this.numeroDoc = numeroDoc;
        this.imagenCliente = imagenCliente;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(int numeroDoc) {
        this.numeroDoc = numeroDoc;
    }

    public String getImagenCliente() {
        return imagenCliente;
    }

    public void setImagenCliente(String imagenCliente) {
        this.imagenCliente = imagenCliente;
    }
}
