package com.monolito.demo.modelo;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import java.util.List;

@Entity
@Table(name = "tipoDocumento")
public class TipoDocumento {
	@Id
	private int id;
	
	@Column(name="tipDoc")
	private String tipDoc;
	
	@OneToMany(targetEntity = Cliente.class, mappedBy = "tipDocumento", orphanRemoval = false, fetch = FetchType.LAZY, 
			   cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
	@Valid
	private List<Cliente> clientes;
	
	public TipoDocumento(){
		
	}
	
	public TipoDocumento(int id, String tipDoc) {
		super();
		this.id = id;
		this.tipDoc = tipDoc;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipDoc() {
		return tipDoc;
	}

	public void setTipDoc(String tipDoc) {
		this.tipDoc = tipDoc;
	}
}
