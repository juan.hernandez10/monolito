package com.monolito.demo.modelo;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;

@Entity
@Table(name = "ciudad")
public class Ciudad {
	@Id
	private int codPostal;
	
	@Column(name="nombre")
	private String nombre;
	
	@OneToMany(targetEntity = Cliente.class, mappedBy = "codCiudad", orphanRemoval = false, fetch = FetchType.LAZY, 
			   cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
	@Valid
	private List<Cliente> clientes;
	
	public Ciudad(){
		
	}
	
	public Ciudad(int codPostal, String nombre) {
		super();
		this.codPostal = codPostal;
		this.nombre = nombre;
	}

	public int getCodPostal() {
		return codPostal;
	}

	public void setCodPostal(int codPostal) {
		this.codPostal = codPostal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
