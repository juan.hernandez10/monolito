package com.monolito.demo.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Fetch;

@Entity
@Table(name = "cliente")
public class Cliente {
	@Id
	private int numeroDoc;
	
	@Column(name="nombres")
	@Valid
	@NotNull(message = "Nombre no puede ser nulo")
	@Size(min = 3, message = "El nombre debe contener mas de 3 o mas caracteres")
	private String nombres;
	
	@Column(name="apellidos")
	@Valid
	@NotNull(message = "Apellidos no puede ser nulo")
	@Size(min = 5, message = "Los apellidos debe contener mas de 5 o mas caracteres")
	private String apellidos;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipDocumento", insertable = true, updatable = false)
	@Fetch(FetchMode.JOIN)
	@Valid
	@NotNull(message = "El tipo de documento no puede ser nulo")
	private TipoDocumento tipDocumento;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "codCiudad", insertable = true, updatable = false)
	@Fetch(FetchMode.JOIN)
	@Valid
	@NotNull(message = "La ciudad no puede ser nulo")
	private Ciudad codCiudad;
	
	@Column(name="edad")
	@Valid
	@NotNull(message = "La edad no puede ser nulo")
	@Min(value = 5, message = "La edad no debe ser menor a 5")
	private int edad;

	public Cliente() {
		
	}

	public Cliente(int numeroDoc, String nombres, String apellidos, TipoDocumento tipDocumento,
				   Ciudad codCiudad, int edad) {
		super();
		this.numeroDoc = numeroDoc;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.tipDocumento = tipDocumento;
		this.codCiudad = codCiudad;
		this.edad = edad;
	}

	public int getNumeroDoc() {
		return numeroDoc;
	}

	public void setNumeroDoc(int numeroDoc) {
		this.numeroDoc = numeroDoc;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public TipoDocumento getTipDocumento() {
		return tipDocumento;
	}

	public void setTipDocumento(TipoDocumento tipDocumento) {
		this.tipDocumento = tipDocumento;
	}

	public Ciudad getCodCiudad() {
		return codCiudad;
	}

	public void setCodCiudad(Ciudad codCiudad) {
		this.codCiudad = codCiudad;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}	
}
