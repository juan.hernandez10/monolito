package com.monolito.demo.excepciones;

import java.util.Date;

public class ErrorGeneral {
	int status;
	String message;
	Date timeStamp;
	String path;
	
	public ErrorGeneral(int status, String message, String path) {
		super();
		this.status = status;
		this.message = message;
		this.timeStamp = new Date();
		this.path = path;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
