package com.monolito.demo.excepciones;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ManejadorExcepciones {
	private static final String ERRORADMIN = "Ocurrió un error favor contactar al administrador.";
	private static final ConcurrentHashMap<String, Integer> CODIGOS_ESTADO = new ConcurrentHashMap<>();

	public ManejadorExcepciones(){
		CODIGOS_ESTADO.put(IllegalArgumentException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
		CODIGOS_ESTADO.put(HttpRequestMethodNotSupportedException.class.getSimpleName(), HttpStatus.METHOD_NOT_ALLOWED.value());
		CODIGOS_ESTADO.put(HttpMessageNotReadableException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
		CODIGOS_ESTADO.put(EntityNotFoundException.class.getSimpleName(), HttpStatus.NOT_FOUND.value());
		CODIGOS_ESTADO.put(NullPointerException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
		CODIGOS_ESTADO.put(TransactionSystemException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> manejadorGlobal(Exception exception, HttpServletRequest request){
		String nombre = exception.getClass().getSimpleName();
		String mensaje = exception.getMessage();
		Integer codigo = CODIGOS_ESTADO.get(nombre);
		if(codigo != null){
			return new ResponseEntity<>(new ErrorGeneral(codigo, mensaje, request.getServletPath()),
					HttpStatus.valueOf(codigo));
		}
		return new ResponseEntity<>(new ErrorGeneral(500, ERRORADMIN, request.getServletPath()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
