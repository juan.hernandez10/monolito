package com.monolito.demo.mapeador;

import com.monolito.demo.dto.AgregarClienteDto;
import com.monolito.demo.dto.ClienteImagenDto;
import com.monolito.demo.dto.ImagenDto;
import com.monolito.demo.modelo.Cliente;
import com.monolito.demo.modelo.Imagen;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import java.util.List;

@Mapper(componentModel = "spring")
public interface Mapeador {
    @Mapping(target = "tipDocumento", ignore = true)
    @Mapping(target = "codCiudad", ignore = true)
    Cliente toCliente(AgregarClienteDto agregarClienteDto);

    @Mapping(source = "codCiudad.nombre", target = "ciudad")
    @Mapping(source = "tipDocumento.tipDoc", target = "tipDocumento")
    @Mapping(target = "imagen", ignore = true)
    ClienteImagenDto toClienteImagenDto(Cliente cliente);

    @Mapping(source = "codCiudad.nombre", target = "ciudad")
    @Mapping(source = "tipDocumento.tipDoc", target = "tipDocumento")
    @Mapping(target = "imagen", ignore = true)
    List<ClienteImagenDto> toClienteImagenDtos(List<Cliente> clientes);

    @Mapping(source = "imagenCliente", target = "imagen")
    ImagenDto toImagenDto(Imagen imagen);
}
