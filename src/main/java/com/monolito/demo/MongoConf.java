package com.monolito.demo;

import com.monolito.demo.repositorios.Rimagen;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackageClasses= Rimagen.class)
public class MongoConf {
}
