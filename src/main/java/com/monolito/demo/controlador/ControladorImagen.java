package com.monolito.demo.controlador;

import com.monolito.demo.servicios.Simagen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin(origins = {"*"})
@RequestMapping("/imagen")
public class ControladorImagen {

    @Autowired
    private Simagen servicio;

    @GetMapping("/documento/{id}")
    public ResponseEntity<Object> obtenerImagenId(@PathVariable Integer id){
        return servicio.obtenerImagenId(id);
    }

    @PostMapping(value = "", consumes = { "multipart/form-data" })
    public ResponseEntity<Object> agregarImagen(@RequestParam("numeroDoc") Integer numeroDoc,
                                                @RequestParam("file") MultipartFile file){
        return servicio.agregarImagen(numeroDoc, file);
    }

    @DeleteMapping("/{id}")
        public ResponseEntity<Object> eliminarImagen(@PathVariable Integer id){
        return servicio.eliminarImagen(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object>  actualizarImagen(@RequestParam("numeroDoc") Integer numeroDoc,
                                                    @RequestParam("file") MultipartFile file){
        return servicio.actualizarImagen(numeroDoc, file);
    }
}
