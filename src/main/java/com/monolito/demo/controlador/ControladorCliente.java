package com.monolito.demo.controlador;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import com.monolito.demo.servicios.Scliente;
import com.monolito.demo.dto.ActualizarClienteDto;
import com.monolito.demo.dto.AgregarClienteDto;

@RestController
@CrossOrigin(origins = {"*"})
@RequestMapping("/cliente")
public class ControladorCliente {
	@Autowired
	private Scliente servicio;
	
	@GetMapping("/documento/{id}")
	public ResponseEntity<Object> clienteId(@PathVariable Integer id){
		return servicio.listarClientesId(id);
	}

	@GetMapping("/tipDoc/{tDoc}")
	public ResponseEntity<Object> clienteTdoc(@PathVariable String tDoc){
		return servicio.listarClientesTipo(tDoc);
	}
	
	@GetMapping("/edad/{edad}")
	public ResponseEntity<Object> clienteEdad(@PathVariable Integer edad){
		return servicio.listarClientesEdad(edad);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminarCliente(@PathVariable Integer id){
		return servicio.eliminarCliente(id);
	}
	
	@PostMapping("")
	public ResponseEntity<Object> agregarCliente(@Valid @RequestBody AgregarClienteDto nuevoCliente){
		return servicio.agregarCliente(nuevoCliente);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Object> actualizarCliente(@Valid @RequestBody ActualizarClienteDto actCliente,
													@PathVariable Integer id){
		return servicio.actualizarCliente(actCliente, id);
	}
}
