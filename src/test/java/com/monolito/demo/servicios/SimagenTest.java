package com.monolito.demo.servicios;

import com.monolito.demo.dto.ImagenDto;
import com.monolito.demo.mapeador.Mapeador;
import com.monolito.demo.modelo.Ciudad;
import com.monolito.demo.modelo.Cliente;
import com.monolito.demo.modelo.Imagen;
import com.monolito.demo.modelo.TipoDocumento;
import com.monolito.demo.repositorios.Rcliente;
import com.monolito.demo.repositorios.Rimagen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
class SimagenTest {

    @Mock
    private Rimagen repoImagen;

    @Mock
    private Rcliente repoCliente;

    @Mock
    private Mapeador mapeador;

    @InjectMocks
    private Simagen servicio;
    private Ciudad ciudadPrueba = new Ciudad(76000, "Cali");
    private TipoDocumento tipoDocPrueba = new TipoDocumento(1, "Tarjeta de Identidad");
    private Cliente cliente1 = new Cliente(12345, "Juan", "Hernandez",
            tipoDocPrueba, ciudadPrueba, 22);
    private Cliente cliente2 = new Cliente(19283, "Natalia", "Vanegas",
            tipoDocPrueba, ciudadPrueba, 15);
    private Imagen imagen = new Imagen(12345, "asdfg12345");
    private static final int numeroDocPrueba = 12345;
    private static final int numeroDocMalPrueba = 12334;
    FileInputStream inputFile = null;
    MockMultipartFile file = null;

    @BeforeEach
    public void configuracion(){
        try{
             this.inputFile = new FileInputStream( "C:\\Users\\juan.hernandez\\Downloads\\DSC_1107.JPG");
             this.file = new MockMultipartFile("file", "DSC_1107",  "multipart/form-data", inputFile);
        }catch (IOException e){

        }
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void obtenerImagenId() {
        ImagenDto dto = new ImagenDto("1a2s23", 12345, "asdfg12345");
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocPrueba)).thenReturn(imagen);
        when(mapeador.toImagenDto(imagen)).thenReturn(dto);
        assertThat(servicio.obtenerImagenId(numeroDocPrueba)).isEqualTo(new ResponseEntity<>(dto, HttpStatus.OK));
    }

    @Test
    void obtenerImagenIdNoCliente(){
        when(repoCliente.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(null);
        assertThat(servicio.obtenerImagenId(numeroDocMalPrueba).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void obtenerImagenIdConClienteNoImagen(){
        when(repoCliente.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(null);
        assertThat(servicio.obtenerImagenId(numeroDocMalPrueba).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void agregarImagen() {
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocPrueba)).thenReturn(null);
        assertThat(servicio.agregarImagen(numeroDocPrueba, file).getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    void agregarImagenNoCliente(){
        when(repoCliente.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(null);
        assertThat(servicio.agregarImagen(numeroDocMalPrueba, file).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void agregarImagenConImagen(){
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocPrueba)).thenReturn(imagen);
        assertThat(servicio.agregarImagen(numeroDocPrueba, file).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void eliminarImagen() {
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocPrueba)).thenReturn(imagen);
        assertThat(servicio.eliminarImagen(numeroDocPrueba).getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void eliminarImagenNoCliente(){
        when(repoCliente.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(null);
        assertThat(servicio.eliminarImagen(numeroDocMalPrueba).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void eliminarImagenNoImagen(){
        when(repoCliente.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(null);
        assertThat(servicio.eliminarImagen(numeroDocMalPrueba).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void actualizarImagen() {
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocPrueba)).thenReturn(imagen);
        assertThat(servicio.actualizarImagen(numeroDocPrueba, file).getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void actualizarImagenNoCliente(){
        when(repoCliente.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(null);
        assertThat(servicio.actualizarImagen(numeroDocMalPrueba, file).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void actualizarImagenNoImagen(){
        when(repoCliente.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(null);
        assertThat(servicio.actualizarImagen(numeroDocMalPrueba, file).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }
}