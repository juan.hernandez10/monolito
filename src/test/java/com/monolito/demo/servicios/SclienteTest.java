package com.monolito.demo.servicios;

import com.monolito.demo.dto.ActualizarClienteDto;
import com.monolito.demo.dto.AgregarClienteDto;
import com.monolito.demo.dto.ClienteImagenDto;
import com.monolito.demo.mapeador.Mapeador;
import com.monolito.demo.modelo.Ciudad;
import com.monolito.demo.modelo.Cliente;
import com.monolito.demo.modelo.Imagen;
import com.monolito.demo.modelo.TipoDocumento;
import com.monolito.demo.repositorios.Rciudad;
import com.monolito.demo.repositorios.Rcliente;
import com.monolito.demo.repositorios.Rimagen;
import com.monolito.demo.repositorios.RtipoDocumento;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.Arrays;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
class SclienteTest {

    @Mock
    private Rcliente repoCliente;

    @Mock
    private Rciudad repoCiudad;

    @Mock
    private RtipoDocumento repoTipoDoc;

    @Mock
    private Rimagen repoImagen;

    @Mock
    private Mapeador mapeador;

    @InjectMocks
    private Scliente servicio;
    private Ciudad ciudadPrueba = new Ciudad(76000, "Cali");
    private TipoDocumento tipoDocPrueba = new TipoDocumento(1, "Tarjeta de Identidad");
    private Cliente cliente1 = new Cliente(12345, "Juan", "Hernandez",
            tipoDocPrueba, ciudadPrueba, 22);
    private Cliente cliente2 = new Cliente(19283, "Natalia", "Vanegas",
            tipoDocPrueba, ciudadPrueba, 15);
    private Imagen imagen = new Imagen(12345, "asdfg12345");
    private static final int numeroDocPrueba = 12345;
    private static final int numeroDocMalPrueba = 12334;

    @BeforeEach
    public void configuracion(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void listarClientesIdSinFoto() {
        ClienteImagenDto dto = new ClienteImagenDto(12345, "Juan", "Hernandez",
                "Cedula Ciudadania", "Cali", 22, null);
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocPrueba)).thenReturn(null);
        when(mapeador.toClienteImagenDto(cliente1)).thenReturn(dto);
        assertThat(servicio.listarClientesId(numeroDocPrueba)).isEqualTo(new ResponseEntity<>(dto, HttpStatus.OK));
    }

    @Test
    void listarClientesIdFoto(){
        ClienteImagenDto dto = new ClienteImagenDto(12345, "Juan", "Hernandez",
                "Cedula Ciudadania", "Cali", 22, null);
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocPrueba)).thenReturn(imagen);
        when(mapeador.toClienteImagenDto(cliente1)).thenReturn(dto);
        dto.setImagen(imagen.getImagenCliente());
        assertThat(servicio.listarClientesId(numeroDocPrueba)).isEqualTo(new ResponseEntity<>(dto, HttpStatus.OK));
    }

    @Test
    void listarClientesIdNoCliente(){
        when(repoCliente.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(null);
        assertThat(servicio.listarClientesId(numeroDocMalPrueba).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void listarClientesTipo() {
        ClienteImagenDto dto1 = new ClienteImagenDto(12345, "Juan", "Hernandez",
                "Tarjeta de Identidad", "Cali", 22, null);
        ClienteImagenDto dto2 = new ClienteImagenDto(19283, "Natalia", "Vanegas",
                "Tarjeta de Identidad", "Cali", 15, null);
        when(repoCliente.findByTipDocumentoTipDoc("Tarjeta de Identidad")).thenReturn(Arrays.asList(cliente1, cliente2));
        when(mapeador.toClienteImagenDtos(Arrays.asList(cliente1, cliente2))).thenReturn(Arrays.asList(dto1, dto2));
        assertThat(servicio.listarClientesTipo("Tarjeta de Identidad"))
                .isEqualTo(new ResponseEntity<>(Arrays.asList(dto1, dto2), HttpStatus.OK));
    }

    @Test
    void listarClientesTipoNoTipo(){
        when(repoCliente.findByTipDocumentoTipDoc("Documento erroneo")).thenReturn(Arrays.asList());
        assertThat(servicio.listarClientesTipo("Documento erroneo").getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void listarClientesEdad() {
        ClienteImagenDto dto1 = new ClienteImagenDto(12345, "Juan", "Hernandez",
                "Tarjeta de Identidad", "Cali", 22, null);
        ClienteImagenDto dto2 = new ClienteImagenDto(19283, "Natalia", "Vanegas",
                "Tarjeta de Identidad", "Cali", 15, null);
        when(repoCliente.findByEdadGreaterThanEqual(15)).thenReturn(Arrays.asList(cliente1, cliente2));
        when(mapeador.toClienteImagenDtos(Arrays.asList(cliente1, cliente2))).thenReturn(Arrays.asList(dto1, dto2));
        assertThat(servicio.listarClientesEdad(15)).isEqualTo(new ResponseEntity<>(Arrays.asList(dto1, dto2), HttpStatus.OK));
    }

    @Test
    void listarClientesEdadNoEdad(){
        when(repoCliente.findByEdadGreaterThanEqual(99)).thenReturn(Arrays.asList());
        assertThat(servicio.listarClientesEdad(99).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void agregarCliente() {
        AgregarClienteDto dto1 = new AgregarClienteDto(12345, "Juan", "Hernandez",
                "Tarjeta de Identidad", "Cali", 22);
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(null);
        when(repoCiudad.findByNombre("Cali")).thenReturn(ciudadPrueba);
        when(repoTipoDoc.findByTipDoc("Tarjeta de Identidad")).thenReturn(tipoDocPrueba);
        when(mapeador.toCliente(dto1)).thenReturn(cliente1);
        assertThat(servicio.agregarCliente(dto1)).isEqualTo(new ResponseEntity<>(cliente1, HttpStatus.CREATED));
    }

    @Test
    void agregarClienteExistente(){
        AgregarClienteDto dto1 = new AgregarClienteDto(12345, "Juan", "Hernandez",
                "Tarjeta de Identidad", "Cali", 22);
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        assertThat(servicio.agregarCliente(dto1).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void actualizarCliente() {
        ActualizarClienteDto dto = new ActualizarClienteDto("Pepito", "Perez");
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        cliente1.setNombres(dto.getNombres());
        cliente1.setApellidos(dto.getApellidos());
        assertThat(servicio.actualizarCliente(dto, numeroDocPrueba)).isEqualTo(new ResponseEntity<>(cliente1, HttpStatus.CREATED));
    }

    @Test
    void actualizarClienteNoCliente(){
        ActualizarClienteDto dto = new ActualizarClienteDto("Pepito", "Perez");
        when(repoCliente.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(null);
        assertThat(servicio.actualizarCliente(dto, numeroDocMalPrueba).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void eliminarClienteSinFoto() {
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocPrueba)).thenReturn(null);
        servicio.eliminarCliente(numeroDocPrueba);
        verify(repoCliente).deleteById(numeroDocPrueba);
        assertThat(servicio.eliminarCliente(numeroDocPrueba)).isEqualTo(new ResponseEntity<>(cliente1, HttpStatus.OK));
    }

    @Test
    void eliminarClienteFoto(){
        when(repoCliente.findByNumeroDoc(numeroDocPrueba)).thenReturn(cliente1);
        when(repoImagen.findByNumeroDoc(numeroDocPrueba)).thenReturn(imagen);
        servicio.eliminarCliente(numeroDocPrueba);
        verify(repoCliente).deleteById(numeroDocPrueba);
        assertThat(servicio.eliminarCliente(numeroDocPrueba)).isEqualTo(new ResponseEntity<>(cliente1, HttpStatus.OK));
    }

    @Test
    void eliminarClienteNoCliente(){
        when(repoCliente.findByNumeroDoc(numeroDocMalPrueba)).thenReturn(null);
        assertThat(servicio.eliminarCliente(numeroDocMalPrueba).getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

}