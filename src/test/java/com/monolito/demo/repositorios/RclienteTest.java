package com.monolito.demo.repositorios;

import com.monolito.demo.modelo.Ciudad;
import com.monolito.demo.modelo.Cliente;
import com.monolito.demo.modelo.TipoDocumento;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class RclienteTest {
    @Autowired
    private Rcliente repoCliente;

    @Autowired
    private Rciudad repoCiudad;

    @Autowired
    private RtipoDocumento repoTipoDoc;
    private Ciudad ciudadPrueba = new Ciudad(76000, "Cali");
    private TipoDocumento tipoDocPrueba = new TipoDocumento(1, "Tarjeta de Identidad");
    private Cliente cliente1 = new Cliente(12345, "Juan", "Hernandez",
            tipoDocPrueba, ciudadPrueba, 22);
    private Cliente cliente2 = new Cliente(19283, "Natalia", "Vanegas",
            tipoDocPrueba, ciudadPrueba, 15);

    @BeforeEach
   void agregarDatos(){
        repoCiudad.save(ciudadPrueba);
        repoTipoDoc.save(tipoDocPrueba);
        repoCliente.save(cliente1);
        repoCliente.save(cliente2);
    }

    @AfterEach
    void eliminarDatos(){
        repoCliente.deleteAll();
        repoCiudad.deleteAll();
        repoTipoDoc.deleteAll();
    }

    @Test
    void findByNumeroDoc() {
        int numDocPrueba = 12345;
        Cliente clienteResult = repoCliente.findByNumeroDoc(numDocPrueba);
        assertThat(clienteResult.getNumeroDoc()).isEqualTo(numDocPrueba);
    }

    @Test
    void findByTipDocumentoTipDoc() {
        String tipoDocPrueba = "Tarjeta de Identidad";
        List<Cliente> listaEsp = Arrays.asList(cliente1, cliente2);
        List<Cliente> listaResult = repoCliente.findByTipDocumentoTipDoc(tipoDocPrueba);
        assertThat(listaResult.get(0).getNumeroDoc()).isEqualTo(listaEsp.get(0).getNumeroDoc());
        assertThat(listaResult.get(1).getNumeroDoc()).isEqualTo(listaEsp.get(1).getNumeroDoc());

    }

    @Test
    void findByEdadGreaterThanEqual() {
        int edadPrueba = 15;
        List<Cliente> listaEsp = Arrays.asList(cliente1, cliente2);
        List<Cliente> listaResult = repoCliente.findByEdadGreaterThanEqual(edadPrueba);
        assertThat(listaResult.get(0).getNumeroDoc()).isEqualTo(listaEsp.get(0).getNumeroDoc());
        assertThat(listaResult.get(1).getNumeroDoc()).isEqualTo(listaEsp.get(1).getNumeroDoc());
    }
}