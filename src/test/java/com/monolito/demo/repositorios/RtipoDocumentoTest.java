package com.monolito.demo.repositorios;

import com.monolito.demo.modelo.TipoDocumento;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class RtipoDocumentoTest {
    @Autowired
    private RtipoDocumento repoTipoDoc;

    @Test
    void findByTipDoc() {
        TipoDocumento tipoDocPrueba = new TipoDocumento(1, "Tarjeta de Identidad");
        repoTipoDoc.save(tipoDocPrueba);
        String tipoPrueba = "Tarjeta de Identidad";
        int idEsperado = 1;
        TipoDocumento tipoEsp = repoTipoDoc.findByTipDoc(tipoPrueba);
        assertThat(tipoEsp.getId()).isEqualTo(idEsperado);
    }
}