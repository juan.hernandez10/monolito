package com.monolito.demo.repositorios;

import com.monolito.demo.modelo.Imagen;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
class RimagenTest {
    @Autowired
    private Rimagen repoImagen;
    private Imagen imagen = new Imagen(12345, "asdfg12345");

    @BeforeEach
    void agregarImagen(){
        repoImagen.save(imagen);
    }

    @AfterEach
    void eliminarImagen(){
        repoImagen.deleteAll();
    }

    @Test
    void findByNumeroDoc() {
        int numeroDocPrueba = 12345;
        Imagen imagenResult = repoImagen.findByNumeroDoc(numeroDocPrueba);
        assertThat(imagenResult.getNumeroDoc()).isEqualTo(imagen.getNumeroDoc());
    }
}