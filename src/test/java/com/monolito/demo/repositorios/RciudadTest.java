package com.monolito.demo.repositorios;

import com.monolito.demo.modelo.Ciudad;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class RciudadTest {
    @Autowired
    private Rciudad repoCiudad;

    @Test
    void findByNombre() {
        Ciudad ciudadPrueba = new Ciudad(76000, "Cali");
        repoCiudad.save(ciudadPrueba);
        String nombrePrueba = "Cali";
        int idEsperado = 76000;
        Ciudad ciudadEsp = repoCiudad.findByNombre(nombrePrueba);
        assertThat(ciudadEsp.getCodPostal()).isEqualTo(idEsperado);
    }
}