insert into ciudad(cod_postal, nombre) values(76000, "Cali");
insert into ciudad(cod_postal, nombre) values(05000, "Medellin");
insert into ciudad(cod_postal, nombre) values(11000, "Bogota");
insert into ciudad(cod_postal, nombre) values(08000, "Barranquilla");

insert into tipo_documento(id, tip_doc) values(1, "Tarjeta de Identidad");
insert into tipo_documento(id, tip_doc) values(2, "Cedula de Ciudadania");
insert into tipo_documento(id, tip_doc) values(3, "Pasaporte");

insert into cliente(numero_doc, nombres, apellidos, tip_documento, cod_ciudad, edad) 
values(12345, "Juan Camilo", "Hernandez Saavedra", 2, 76000, 22);
insert into cliente(numero_doc, nombres, apellidos, tip_documento, cod_ciudad, edad) 
values(67891,  "Santiago",  "Andrade Ramirez", 1, 05000, 15);
insert into cliente(numero_doc, nombres, apellidos, tip_documento, cod_ciudad, edad)
values(19283, "Natalia", "Vanegas", 2, 11000, 25);
insert into cliente(numero_doc, nombres, apellidos, tip_documento, cod_ciudad, edad)
values(37692, "Sofia", "Lopez", 3, 08000, 20);

select * from cliente;