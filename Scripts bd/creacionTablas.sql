create table tipoDocumento(
	id INT AUTO_INCREMENT PRIMARY KEY,
    tipoDoc varchar(100)
);

create table ciudad(
	codPostal BIGINT PRIMARY KEY,
    nombre varchar(200)
);

create table cliente(
	numeroDoc BIGINT primary key,
    nombres varchar(200),
    apellidos varchar(200),
    tipDocumento int,
    codCiudad bigint,
    edad int,
    foreign key(codCiudad) references ciudad(codPostal),
    foreign key(tipDocumento) references tipodocumento(id)
);

